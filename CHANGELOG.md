# (2020-03-07)

### Features

- **dev:** add changelog ([c33ab1d](https://gitlab.com/mlaradji/jsonschema-to-python/commit/c33ab1d))
- **dev:** add configuration files ([181f63c](https://gitlab.com/mlaradji/jsonschema-to-python/commit/181f63c))
- **jsonschema:** add support for nested objects ([4bc11b2](https://gitlab.com/mlaradji/jsonschema-to-python/commit/4bc11b2))
