# jsonschema-to-python

Create python source code from JSON schema.

## Features
- Generates python source code as print output. Redirecting console output to a file allows one to use the JSON schema as a type hint:
    ```bash
    python jspc/__init__.py > schema.py
    ```
- Output classes have optional and required keys correctly set.
- Follows internal '$ref' references, and detects reference cycles.
- Plot JSON Schema structure.
- Supported keywords:
    - [x] `'properties'`
    - [x] `'required'`
    - [ ] `'additionalProperties'`
    - [ ] `'oneOf'`
    - [ ] `'allOf'`
    - [ ] `'anyOf'`
    - [ ] `'enum'`
    - [ ] `'pattern'`
    - [ ] `'description'`
    - [ ] `'title'`
    - [ ] `'$ref'`
        - [x] `'#/...'` (internal references)
        - [ ] `'https://'` (external references)
    - [ ] `'$id'`
    - [ ] `'default'`
    